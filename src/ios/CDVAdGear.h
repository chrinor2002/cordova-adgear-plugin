//
//  CDVAdGear.h
//

#import <Foundation/Foundation.h>
#import "AdGear.h"
#import "AGSpotView.h"
#import <Cordova/CDV.h>

@interface CDVAdGear : CDVPlugin <AGSpotViewDelegate> {
}

@property (nonatomic, retain) AGSpotView* bannerView;
@property (assign) BOOL bannerIsVisible;
@property (assign) BOOL bannerIsInitialized;
@property (assign) BOOL bannerAtTop;
@property (assign) BOOL bannerOverlap;
@property (assign) BOOL offsetTopBar;

- (void) createBannerView:(CDVInvokedUrlCommand *)command;
- (void) showAd:(CDVInvokedUrlCommand *)command;

@end
